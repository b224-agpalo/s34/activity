const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/home", (request, response) => {

    response.send("Welcome to home page!")
});

let users = [{
    username: "johndoe",
    password: "johndoe1234"
},
{
    username: "janesmith",
    password: "janesmith1234"
}
];

app.get("/users", (request, response) => {

    response.send(users)
});

app.delete("/delete-user", (request, response) => {

    let message;

    for (let i = 0; i < users.length; i++) {

        if (request.body.username == users[i].username && request.body.password == users[i].password) {
            users[i] = request.body;

            message = `User ${request.body.username}'s profile has been deleted.`
            break;
        } else {
            message = "User does not exist or username/password is wrong"
        }
    }
    response.send(message);
});

app.listen(port, () => console.log(`Server running at port ${port}`));
